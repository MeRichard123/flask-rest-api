from flask import Flask
from flask_restful import Api, Resource, reqparse, abort, fields, marshal_with
from flask_sqlalchemy import SQLAlchemy

# Create Flask App
app = Flask(__name__)
# Initialise the API
api = Api(app)
# Set up the database configuration
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
# Connect to data base
db = SQLAlchemy(app)


# Set up the database model
class VideoModel(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    likes = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return f"Video(name = {name}, likes = {likes})"


# Get the argument from the url /video/argument
video_put_args = reqparse.RequestParser()

# Set up the PUT argument types and the message if there is none
video_put_args.add_argument(
    "name", type=str, help="Name of the video is required", required=True)
video_put_args.add_argument(
    "likes", type=int, help="Likes on the video", required=True)


# Initialise Argument Parser
video_update_args = reqparse.RequestParser()

# Set arguments for PATCH method
video_update_args.add_argument(
    "name", type=str, help="Name of the video is required")
video_update_args.add_argument("likes", type=int, help="Likes on the video")

# Set up how it will be represented when returned
resource_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'likes': fields.Integer
}

# HTTP methods are defined in a class from the flask rest framework


class Video(Resource):
    # Decorate with the fields to display the response correctly
    @marshal_with(resource_fields)
    def get(self, video_id):
        # Look for video by id in database
        result = VideoModel.query.filter_by(id=video_id).first()

        if not result:
            abort(404, message="Could not find video with that id")
        return result

    @marshal_with(resource_fields)
    def put(self, video_id):
        args = video_put_args.parse_args()
        result = VideoModel.query.filter_by(id=video_id).first()
        if result:
            abort(409, message="Video id taken...")

        # Create a new video via the model
        video = VideoModel(
            id=video_id, name=args['name'], likes=args['likes'])
        # add the video and commit changes to database
        db.session.add(video)
        db.session.commit()
        # return the new video with a status of 201
        return video, 201

    # PATCH updates an existing video
    @marshal_with(resource_fields)
    def patch(self, video_id):
        args = video_update_args.parse_args()
        result = VideoModel.query.filter_by(id=video_id).first()
        if not result:
            abort(404, message="Video doesn't exist, cannot update")
        if args['name']:
            result.name = args['name']
        if args['likes']:
            result.likes = args['likes']

        db.session.commit()

        return result

    @marshal_with(resource_fields)
    def delete(self, video_id):
        res = VideoModel.query.filter_by(id=video_id).first()
        db.session.delete(res)
        db.session.commit()
        return '', 204


class AllVideos(Resource):
    @marshal_with(resource_fields)
    def get(self):
        all_prods = VideoModel.query.all()
        return all_prods, 201


# Set up the routes
api.add_resource(Video, "/video/<int:video_id>")
api.add_resource(AllVideos, "/videos")

if __name__ == "__main__":
    app.run(debug=True)
