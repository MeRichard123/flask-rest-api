# Usage Instructions:

- I suggest creating a virtual environment to get this up and running. 

> pip install virtualenv
> virtualenv env

If your on windows start the virtual environment like this:
> env\Scripts\activate.bat
If your on Mac I belive its bin/activate

> pip install -r reqirements.txt

Then to run the server:

> python main.py

You can then use the test.py file to run requests to the api
Alternatively use something like the postman desktop app 