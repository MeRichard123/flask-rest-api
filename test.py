import requests

BASE = "http://127.0.0.1:5000/"

data = [
    {"name": "Django for beginner", "likes": 78},
    {"name": "Flask for beginners", "likes": 7408},
    {"name": "Redux for beginners", "likes": 1898}
]


def print_data():
    for i in range(len(data)):
        result = requests.get(BASE+"video/"+str(i))
        print(result.json())


for i in range(len(data)):
    res = requests.put(BASE + "video/" + str(i),
                       data[i]).json()
    print(res)


input()
print_data()


# delete
res = requests.delete(BASE + "video/" + str(1))
print(res)


input()
print_data()

# update
res = requests.patch(
    BASE + "video/2", {"name": "Redux for advanced peeps", 'likes': 1568096}).json()
print(res)
print_data()
